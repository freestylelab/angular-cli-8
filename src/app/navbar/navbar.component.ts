import { Component, OnInit } from '@angular/core';

export class Elementi {
  constructor(
    public link: string,
    public iconClass: string,
    public title: string
  )  { }
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  elementi = [
    new Elementi('/index', 'oi oi-home', 'home', ),
    new Elementi('/articoli', 'oi oi-box', 'articoli')
  ];

  constructor() { }

  ngOnInit() {
  }

  // htmlGeneration() {
  //   this.items.forEach((item) => {
  //     this.output += '<li class="nav-item">' +
  //                 '<a [routerLink]="[' + item.url + ']" class="nav-link">' +
  //                   '<span class=' + item.spanClass + 'title="' + item.title + '"aria-hidden="true"></span>' +
  //                 item.title + '</a>' +
  //               '</li>';
  //   });
  // }

}
